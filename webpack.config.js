var webpack = require('webpack');

module.exports = {

    entry: [
		"./src/public/js/controllers/analyze.js",
		"./src/public/js/controllers/camera.js",
		"./src/public/js/controllers/list.js",
		"./src/public/js/controllers/index.js",
		"./src/public/js/models/map.js"
    ],

    output: {
	path: __dirname + "/src/public/dist",
	filename: 'bundle.js'
    },

    module: {
	rules: [
	    {
		test: /\.css$/,
		use: [
		    {loader: 'style-loader'},
		    {loader: 'css-loader'}
		]
	    }
	]
    },

    plugins: [
		new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
		})
    ]

};
