import subprocess
import uuid
import sys
import os.path
import time

def suniqid():
    return (str(uuid.uuid4().fields[-1])[:10]);

def genUriTab(filename):
    uriTab = [];
    with open(filename) as file:
        for line in file:
            uriTab.append(line.rstrip());
    return (uriTab);
            
def takeScreenshots(uriTab):

    while True:
      for uri in uriTab:
        print(uri);
        screenshotName = 'screenshots/' + suniqid() + '.jpg';
        ffmpegArgs = ['ffmpeg',
                      '-f', 'MJPEG',
                      '-y',
                      '-i', uri,
                      '-r', '1',
                      '-vframes', '1',
                      '-q:v', '1',
                      screenshotName];
        subprocess.run(ffmpegArgs);
      time.sleep(30);

if __name__ == "__main__":
    if len(sys.argv) == 2:
        if (os.path.isfile(sys.argv[1])):
            if not os.path.isdir('screenshots'):
                os.mkdir('screenshots');
            uriTab = genUriTab(sys.argv[1]);
            takeScreenshots(uriTab);
    else:
        print('usage: ./main.py <filename>');
