/*
 * Add cameras from list to Cosmos DB
 */

/* Utils ------------------------------------------------------------------------ */
function addCamerasFromFile(APIEnv) {

    const file = require('fs').createReadStream(APIEnv.filename);
    const lineReader = require('readline').createInterface({
		input: file
    });
    let i = 0;
    
    lineReader.on('line', (url) => {
    	console.log('=> ' + url);
		APIEnv.req.params.url = url;
		APIEnv.resources(APIEnv.db, APIEnv.req, APIEnv.res);
		i++;
    });

    lineReader.on('close', () => {
		process.exit();
    });

};

/* Database --------------------------------------------------------------------- */
const mongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/cosmos';

if (process.argv.length > 2)
{
    mongoClient.connect(url, (err, database) => {
		if (err) throw err;
		const db = database.db('cosmos');
		const resources = require('./../../src/resources');
		const http = require('http');
		const filename = String(process.argv[2]);
		const fs = require('fs');
		
		/* Verification --------------------------------------------------------- */
		if (fs.existsSync(filename))
		{

		    /* Define API environment ------------------------------------------- */
		    const APIEnv = {
				'db': db,
				'req': {
				    "params": {
					"url": undefined
				    }
				},
				'res': {
				    "json": () => {}
				},
				'filename': filename,
				'resources': resources.Cameras.addCamera
		    }

		    /* Emulate API environment -------------------------------------------- */
		    addCamerasFromFile(APIEnv);

		} else {
		    console.error('Cosmos: faddcam: Unknown file');
		    database.close();
		}
    });
} else console.error('usage: node faddcam <camlist>');
/* tpoac@student.42.fr ---------------------------------------------------------- */
    
