module.exports = class Sequencer {

	constructor(db, ioSeq, camera, activeDetection) {
		const id = camera.value._id;
		const publicDetectionDir = '/sequences/detection/' + id.toString();
		const publicOriginalDir = '/sequences/original/' + id.toString();
		const originalDir = 'src/public/sequences/original/' + id.toString();
		const detectionDir = 'src/public/sequences/detection/' + id.toString();
		const redis = require('redis');
    	const client = redis.createClient();

		this.ioSeq = ioSeq;
		this.camera = camera;
		this.url = camera.value.geojson.properties.url;
		this.id = id;
		this.db = db;
		this.publicDetectionDir = publicDetectionDir;
		this.publicOriginalDir = publicOriginalDir;
		this.originalDir = originalDir;
		this.detectionDir = detectionDir;
		this.activeDetection = activeDetection;
		this.originalClientPath = 'public/sequences/original/' + id.toString();
		this.detectionClientPath = 'public/sequences/detection/' + id.toString();
		if (activeDetection == true) this.initDarknet();
    	this.verifyDirs(this.originalDir, this.detectionDir);
	}

	uniqid() {
		return (new Date().getTime() + Math.floor((Math.random() * 10000) + 1)).toString(16);
    };

    verifyDirs() {
    	const fs = require('fs');

    	if (fs.existsSync('src/public/sequences'))
		{
			if (!fs.existsSync(this.originalDir))
				fs.mkdirSync(this.originalDir);
			if (!fs.existsSync(this.detectionDir))
				fs.mkdirSync(this.detectionDir);
		}
		else {
			if (!fs.existsSync('src/public/sequences/original'))
				fs.mkdirSync('src/public/sequences/original');
			if (!fs.existsSync('src/public/sequences/detection'))
				fs.mkdirSync('src/public/sequences/detection');
		}
    }

    getResult(output) {
    	var split = output.split("\n");
    	var pourc = [];
    	var now = new Date();
		var i = 0;
		var objects = 0;

    	while (split[i])
    	{
    		if (split[i] != "Output" && split[i] != "Input" && split[i][0] != '.') {
    			pourc.push(split[i]);
    			objects++;
    		}
    		i++;
    	}
    	var detectionResult = {
    		date: {
    			year: now.getFullYear(),
    			month: now.getMonth() + 1,
    			day: now.getDate()
    		},
    		time: {
    			hours: now.getHours(),
    			minutes: now.getMinutes(),
    			seconds: now.getSeconds()
    		},
    		total: objects,
    		pourc: pourc
    	}
    	return (detectionResult);
    }

    initDarknet() {
    	const { spawn } = require('child_process');
    	const redis = require('redis');
    	const client = redis.createClient();

	    /* Initialize darknet process */
	    process.chdir('darknet');
		const darknet = spawn('./darknet', [
			'detect',
			'cfg/marine.cfg',
			'weights/marine.weights']);
		process.chdir('..');

		// Save in Redis DB
		client.set(this.id.toString(), darknet.pid);

		darknet.stdout.on('data', (data) => {
			const fs = require('fs');

			if (data.toString().indexOf("Output") != -1) {

				/* Wait screenshot generation on HD */
				while (true) {
					if (fs.existsSync(this.detectionDir + '/' + this.screenshotName)) {

						var monitoringName = '<span class="badge">' + this.camera.value.geojson.properties.country + '</span> - Unknown city';

						if (this.camera.value.geojson.properties.city != '')
							monitoringName = '<span class="badge">' + this.camera.value.geojson.properties.country + '</span> - ' + this.camera.value.geojson.properties.city;

						/* Stock datas in DB : 
						 * - original path
						 * - detection path
						 * - result (datetime and infos detections)
						 * - camera id 
						 */
						const seqDatas = {
							original: this.publicOriginalDir + '/' + this.screenshotName,
							detection: this.publicDetectionDir + '/' + this.screenshotName,
							result: this.getResult(data.toString()),
							name: monitoringName,
							id: this.id
						};
						this.db.collection("sequences").insertOne(seqDatas, (err, res) => {
							if (err) throw err;
						});

						/* Send socket for monitoring managment */
						this.ioSeq.emit('newSequence', {
				    		id: this.id,
							original: this.publicOriginalDir + '/' + this.screenshotName,
							detection: this.publicDetectionDir + '/' + this.screenshotName,
							result: this.getResult(data.toString()),
							name: monitoringName
						});
						break ;
					}
				}
			}

			/* Output, take a screenshot and write input */
			if (data.toString().indexOf("Input") != -1) {
				this.screenshotName = this.uniqid() + '.jpg';
				this.originalPath = this.originalDir + '/' + this.screenshotName;

				const { spawn } = require('child_process');
				const ffmpeg = spawn(
					'ffmpeg', ['-f', 'MJPEG',
								'-y',
								'-i', this.url,
								'-r', '1',
								'-vframes', '1',
								'-q:v', '1',
								this.originalPath]);

				ffmpeg.on('close', (code) => {
					if (this.activeDetection == true) {
					    darknet.stdin.write('../' + this.originalPath + "\n");
					}
				});
			}
		});
    }

}