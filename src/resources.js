module.exports.Cameras = class Cameras {

/* Cameras -------------------------------------------------------------- */

    /* [GET] /api/cameras --------------------------------------------- */
    static getCameras(db, req, res, render, apiUse)
    {
		db.collection('cameras').find({}).toArray(
			(err, cameras) => {
				if (err) throw err;

				if (apiUse == true) res.json(cameras);
				if (render) render(cameras);
			});
    }

    /* [GET] /api/cameras/geojson ------------------------------------- */
    static getCamerasGeoJSON(db, req, res, render, apiUse)
    {
		db.collection('cameras').find({}).toArray(
			(err, cameras) => {
				if (err) throw err;

				const camerasGeoJSON = [];	    
				var ret;

				cameras.forEach((camera) => {
				    camera.geojson.properties._id = camera._id;
				    camerasGeoJSON.push(camera.geojson);
				});
				ret = {
				    "type": "FeatureCollection",
				    "features": camerasGeoJSON
				};
				if (apiUse == true) res.json(ret);
				if (render) render(ret);
			});
    }

    /* [GET] /api/cameras/geojson/{id} ------------------------------------- */
    static getCameraGeoJSON(db, req, res, render, apiUse)
    {
		const ObjectID = require('mongodb').ObjectID;

		if (ObjectID.isValid(req.params.id))
		{
		    db.collection('cameras').find({ '_id': ObjectID(req.params.id) }).toArray(
		    	(err, cameras) => {
				    if (err) throw err;

				    const cameraGeoJSON = [];	    
				    var ret = {};
				    
				    cameras.forEach((camera) => {
						camera.geojson.properties._id = camera._id;
						cameraGeoJSON.push(camera.geojson);
				    });
				    ret = ({
						"type": "FeatureCollection",
						"features": cameraGeoJSON
				    });
				    if (apiUse == true) res.json(ret);
				    if (render) render(ret);
				});
		}
    }

    /* [GET] /api/cameras/{id} ---------------------------------------- */
    static getCamera(db, req, res, render, apiUse)
    {
		const ObjectID = require('mongodb').ObjectID;

		if (ObjectID.isValid(req.params.id))
		{
		    db.collection('cameras').findOne({ '_id': ObjectID(req.params.id) },
		    	(err, camera) => {
			    	if (err) throw err;

			    	if (apiUse == true) res.json(camera);
			    	if (render) render(camera);
				});
		}
    }

    /* [POST] /api/cameras/{url} -------------------------------------- */
    static addCamera(db, req, res, render, apiUse)
    {
		const { URL } = require('url');
		const url = new URL(req.params.url);
		const geoip = require('geoip-lite');
		const geoIpLookup = geoip.lookup(url.hostname);
		const geojson = require('geojson');

		if (geoIpLookup != undefined)
		{
		    const geoDatas = [{
				url: url.href,
				country: geoIpLookup.country,
				city: geoIpLookup.city,
				zip: geoIpLookup.zip,
				resgion: geoIpLookup.region,
				range: geoIpLookup.range,
				lat: geoIpLookup.ll[0],
				lng: geoIpLookup.ll[1]
		    }];

		    db.collection('cameras').save({
		    	geojson: geojson.parse(geoDatas, { Point: ['lat', 'lng'] }).features[0],
		    	detection: 0
			}, (err, dbResult) => {
			    if (err) throw err;

			    if (apiUse == true) res.json(dbResult);
			    if (render) render(dbResult);
			});
		}
    }

    /* [PUT] /api/cameras/detection/{value} --------------------------- */
    static putCameraDetection(db, req, res, ioSeq, apiUse)
    {
    	const redis = require('redis');
		const client = redis.createClient();

		client.exists(req.params.id, (err, reply) => {
			if (err) throw err;

			const ObjectID = require('mongodb').ObjectID;
			const value = req.params.value;
			const Sequencer = require('./sequencer');
			const { spawn } = require('child_process');

			db.collection('cameras').findOneAndUpdate(
				{ '_id': ObjectID(req.params.id) },
				{ $set: { 'detection': value } },
				{ new: true },
				(err, camera) => {

					if (err) throw err;

					if (value == 1 && reply != 1) {
						new Sequencer(db, ioSeq, camera, true);
					}
					else if (reply === 1) {
						// Deactivate detection from id:pid
						client.get(camera.value._id.toString(), function(err, reply_pid) {
							if (err) throw err;

							client.del(camera.value._id.toString(), function(err, reply_del) {
								if (err) throw err;

								if (reply_del == 1)
									process.kill(reply_pid);
							});
						});
					}

					if (apiUse == true) res.json(camera);
				});
		});
    }

    /* [DELETE] /api/cameras/{id} ------------------------------------ */
    static deleteCamera(db, req, res, render, apiUse)
    {
		const ObjectID = require('mongodb').ObjectID;

		db.collection('cameras').remove(
			{ '_id': ObjectID(req.params.id) },
			(err, dbResult) => {
				if (err) throw err;

				if (apiUse) res.json(dbResult);
				if (render) render(dbResult);
			});
    }

    /* [DELETE] /api/cameras ------------------------------------------ */
    static deleteCameras(db, req, res, render, apiUse)
    {
		db.collection('cameras').remove({},
			(err, dbResult) => {
				if (err) throw err;

				if (apiUse) res.json(dbResult);
				if (render) render(dbResult);
			});
    }

/* Sequences ------------------------------------------------------------ */

    /* [GET] /api/sequences/{id} ---------------------------------------- */
    static getSeqByCamId(db, req, res, render, apiUse) {
    	const ObjectID = require('mongodb').ObjectID;

		if (ObjectID.isValid(req.params.id))
		{
		    db.collection('sequences').find({ id: ObjectID(req.params.id) }).toArray((err, sequences) => {
		    	if (err) throw err;

			    if (apiUse) res.json(sequences);
			    if (render) render(sequences);
			});
		}
    }

    /* [GET] /api/sequences ---------------------------------------- */
    static getSequences(db, req, res, render, apiUse) {
		db.collection('sequences').find({}).toArray((err, sequences) => {
		    if (err) throw err;

			if (apiUse) res.json(sequences);
			if (render) render(sequences);
		});
    }

   	/* [DELETE] /api/sequences ------------------------------------------ */
    static deleteAllSeq(db, req, res, render, apiUse)
    {
		db.collection('sequences').remove({},
			(err, dbResult) => {
				if (err) throw err;

				if (apiUse) res.json(dbResult);
				if (render) render(dbResult);
			});
    }
    
}
