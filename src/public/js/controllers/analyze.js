/* Requires --------------------------------------------------------------- */

import { map } from '../models/map.js';

/* Load datas cameras ----------------------------------------------------- */

var controllerAnalyze = function() {

/* Initialize map --------------------------------------------------------- */

    map('http://127.0.0.1:8080/api/cameras/geojson', [0, 0], 4);

}

/* Export ----------------------------------------------------------------- */

window.controllerAnalyze = controllerAnalyze;

/* tpoac@student.42.fr ---------------------------------------------------- */
