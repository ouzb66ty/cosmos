import io from 'socket.io-client/dist/socket.io.js';

var controllerIndex = function() {
	var sequencesIo = io.connect('http://localhost:8080');

    /* For detections Box */
	function newTBodyDetection(screenshot) {
		var tbody = document.createElement("tbody");
		var tr = document.createElement("tr");
		var id = document.createElement("th");
		var taux = document.createElement("td");
		var datetime = document.createElement("td");
		var detection = document.createElement("td");
		var original = document.createElement("td");
		var total = document.createElement("td");

		console.dir(screenshot);
		id.innerHTML = '<a target="_blank" href="/camera/' + screenshot.id + '">' + screenshot.name + '</a>';
		taux.innerHTML = screenshot.result.pourc.join(", ");
		detection.innerHTML = '<a target="_blank" href="' + screenshot.detection + '">Voir la détection</a>';
		original.innerHTML = '<a target="_blank" href="' + screenshot.original + '">Voir le screenshot</a>';
		datetime.innerHTML = screenshot.result.date.day + '/' + screenshot.result.date.month + '/' + screenshot.result.date.year + ' - ' + screenshot.result.time.hours + ':' + screenshot.result.time.minutes + ':' + screenshot.result.time.seconds;
		total.innerHTML = screenshot.result.total;
		tr.appendChild(id);
		tr.appendChild(taux);
		tr.appendChild(detection);
		tr.appendChild(original);
		tr.appendChild(datetime);
		tr.appendChild(total);
		if (screenshot.result.pourc.join(", ").search("submarine") != -1 || screenshot.result.pourc.join(", ").search("fregate") != -1 || screenshot.result.pourc.join(", ").search("aircraft-carrier") != -1 || screenshot.result.pourc.join(", ").search("supply-tanker") != -1)
			tr.style.backgroundColor = 'rgba(255, 0, 0, 0.3)';
		else if (screenshot.result.pourc.join(", ").search("boat") != -1)
			tr.style.backgroundColor = 'rgba(0, 255, 0, 0.3)';
		tbody.appendChild(tr);
		return (tbody);
	}

	sequencesIo.on('newSequence', function (data) {
		console.dir(data);
		const screenshot = data;
		const detectionsBox = document.querySelector('#detectionsBox table');
		const detectionTBody = newTBodyDetection(screenshot);

		if (screenshot.result.total > 0)
			detectionsBox.insertBefore(detectionTBody, detectionsBox.firstChild);
    });

};

window.controllerIndex = controllerIndex;