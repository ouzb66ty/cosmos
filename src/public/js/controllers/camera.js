/* Requires --------------------------------------------------------------- */

import { map } from '../models/map.js';
import Chart from 'chart.js';
import io from 'socket.io-client/dist/socket.io.js';

/* Load datas cameras ----------------------------------------------------- */

var controllerCamera = function(id) {

    var pathname = window.location.pathname;
    var filename = pathname.substring(pathname.lastIndexOf('/') + 1);
    var xhttp = new XMLHttpRequest();
    var btnDetectionOn = document.querySelector("#btnDetectionOn");
    var btnDetectionOff = document.querySelector("#btnDetectionOff");
    var sequencesIo = io.connect('http://localhost:8080');
    var sequencesImg = document.querySelectorAll("#gallery .col-sm-3");
    const close = document.querySelector('#close');
    const switcher = document.querySelector('#switch');
    const imgFullscreen = document.querySelector('#fullscreen img');

/* Initialize map --------------------------------------------------------- */

    /* BAD ISSUE : Two Requests */

    close.addEventListener("click", function() {
    	fullscreen.style.display = "none";
		gallery.style.display = "flex";
    });

    /* DETECTION to ORIGINAL (SWITCHER) */
	switcher.addEventListener("click", function() {
		if (imgFullscreen.src.search("detection") != -1) {
			imgFullscreen.src = imgFullscreen.src.replace("detection", "original");
		} else if (imgFullscreen.src.search("original") != -1){
			imgFullscreen.src = imgFullscreen.src.replace("original", "detection");
		}
    });

/* Events ----------------------------------------------------------------- */
    
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var camera = JSON.parse(this.responseText);
			
			map('http://127.0.0.1:8080/api/cameras/geojson/' + id, camera.geojson.geometry.coordinates, 15);
		}
    };
    xhttp.open("GET", "/api/cameras/" + id, true);
    xhttp.send();

    /* Detection On */
    btnDetectionOn.addEventListener("click", function() {

		var request = '/api/cameras/detection/' + id + '/1';

		btnDetectionOn.className = "list-group-item list-group-item-action list-group-item-primary";
		btnDetectionOff.className = "list-group-item list-group-item-action list-group-item-light";
		xhttp.onreadystatechange = undefined;
		xhttp.open("PUT", request, true);
		xhttp.send();

    });

    /* Detection Off */
    btnDetectionOff.addEventListener("click", function() {

		var request = '/api/cameras/detection/' + id + '/0';

		btnDetectionOn.className = "list-group-item list-group-item-action list-group-item-light";
		btnDetectionOff.className = "list-group-item list-group-item-action list-group-item-primary";
		xhttp.onreadystatechange = undefined;
		xhttp.open("PUT", request, true);
		xhttp.send();

    });

	/* CHARTS (number detections and max) */
	/* CHARTS (number detections and max) */
	/* CHARTS (number detections and max) */

	var ctx = document.getElementById("numberDetectionsChart");

	var numberDetectionsChart = new Chart(ctx, {
		type: 'line',
		labels: [],
		data: {
			datasets: [{
		        label: 'Boat',
		        data: [],
		        borderColor: [
		            '#4cd137'
		        ],
		        fill: false
		    },
		    {
		        label: 'Submarine',
		        data: [],
		        borderColor: [
		            '#00a8ff'
		        ],
		        fill: false
		    },
		    {
		        label: 'Fregate',
		        data: [],
		        borderColor: [
		            '#e84118'
		        ],
		        fill: false
		    },
		    {
		        label: 'Aircraft-Carrier',
		        data: [],
		        borderColor: [
		            '#9c88ff'
		        ],
		        fill: false
		    },
		    {
		        label: 'Supply-Tanker',
		        data: [],
		        borderColor: [
		            '#fbc531'
		        ],
		        fill: false
		    }]
		},
		options: {
			scales: {
	        	yAxes: [{
                	display: true,
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 5
                    }
                }]
	    	}
	    }
	});

	sequencesIo.on('newSequence', function (data) {
		if (id == data.id) {
			var a = 0;
			var i = 0;
			var j = 0;
			var k = 0;
			var l = 0;
			var m = 0;

			while (a < data.result.pourc.length) {
				if (data.result.pourc[a].search('boat') != -1)
					i++;
				else if (data.result.pourc[a].search('submarine') != -1)
					j++;
				else if (data.result.pourc[a].search('fregate') != -1)
					k++;
				else if (data.result.pourc[a].search('aircraft-carrier') != -1)
					l++;
				else if (data.result.pourc[a].search('supply-tanker') != -1)
					m++;
				a++;
			}
			numberDetectionsChart.data.labels.push(data.result.date.day + '/' + data.result.date.month + '/' + data.result.date.year + ' - ' + data.result.time.hours + ':' + data.result.time.minutes + ':' + data.result.time.seconds);
			numberDetectionsChart.data.datasets[0].data.push(i);
			numberDetectionsChart.data.datasets[1].data.push(j);
			numberDetectionsChart.data.datasets[2].data.push(k);
			numberDetectionsChart.data.datasets[3].data.push(l);
			numberDetectionsChart.data.datasets[4].data.push(m);
			numberDetectionsChart.update();
		}
	});
	/* CHARTS (number detections and max) */
	/* CHARTS (number detections and max) */
	/* CHARTS (number detections and max) */

	/* For detections Box */
	function newTBodyDetection(screenshot) {
		var tbody = document.createElement("tbody");
		var tr = document.createElement("tr");
		var id = document.createElement("th");
		var datetime = document.createElement("td");
		var detection = document.createElement("td");
		var original = document.createElement("td");
		var total = document.createElement("td");

		id.innerHTML = screenshot.result.pourc.join(", ");
		detection.innerHTML = '<a target="_blank" href="' + screenshot.detection + '">Voir la détection</a>';
		original.innerHTML = '<a target="_blank" href="' + screenshot.original + '">Voir le screenshot</a>';
		datetime.innerHTML = screenshot.result.date.day + '/' + screenshot.result.date.month + '/' + screenshot.result.date.year + ' - ' + screenshot.result.time.hours + ':' + screenshot.result.time.minutes + ':' + screenshot.result.time.seconds;
		total.innerHTML = screenshot.result.total;
		tr.appendChild(id);
		tr.appendChild(detection);
		tr.appendChild(original);
		tr.appendChild(datetime);
		tr.appendChild(total);
		if (screenshot.result.pourc.join(", ").search("submarine") != -1 || screenshot.result.pourc.join(", ").search("fregate") != -1 || screenshot.result.pourc.join(", ").search("aircraft-carrier") != -1 || screenshot.result.pourc.join(", ").search("supply-tanker") != -1)
			tr.style.backgroundColor = 'rgba(255, 0, 0, 0.3)';
		else if (screenshot.result.pourc.join(", ").search("boat") != -1)
			tr.style.backgroundColor = 'rgba(0, 255, 0, 0.3)';
		tbody.appendChild(tr);
		return (tbody);
	}

    sequencesIo.on('newSequence', function (data) {
		if (data.id == id)
		{
			const screenshot = data;
			const detectionImg = document.createElement('img');
			const detectionA = document.createElement('a');
			const gallery = document.querySelector('#gallery');
			const colsm4 = document.createElement('div');
			const fullscreen = document.getElementById('fullscreen');
			const infoDiv = document.createElement('p');
			const detectionsBox = document.querySelector('#detectionsBox table');
			const detectionTBody = newTBodyDetection(screenshot);

			colsm4.className = "col-sm-2 seqGallery";
			detectionImg.src = screenshot.detection;
			detectionA.innerHTML = '';
			detectionA.appendChild(detectionImg);
			colsm4.appendChild(detectionA);
			infoDiv.innerHTML = screenshot.result.date.day + '/' + screenshot.result.date.month + '/' + screenshot.result.date.year + ' - ' + screenshot.result.time.hours + ':' + screenshot.result.time.minutes + ':' + screenshot.result.time.seconds;
			colsm4.appendChild(infoDiv);
			gallery.insertBefore(colsm4, gallery.firstChild);
			if (screenshot.result.total > 0)
				detectionsBox.insertBefore(detectionTBody, detectionsBox.firstChild);

			detectionA.addEventListener("click", function(code) {
				imgFullscreen.src = code.target.attributes.src.nodeValue;
				fullscreen.style.display = "block";
				gallery.style.display = "none";
    		});
		}
    });
}

/* Export ----------------------------------------------------------------- */

window.controllerCamera = controllerCamera;