/* Requires --------------------------------------------------------- */

import 'bootstrap'
import 'ol/ol.css';
import Map from 'ol/map';
import TileLayer from 'ol/layer/tile';
import OSM from 'ol/source/osm';
import VectorLayer from 'ol/layer/vector';
import VectorSource from 'ol/source/vector';
import GeoJSON from 'ol/format/geojson.js';
import Proj from 'ol/proj.js';
import View from 'ol/view';

export function map(geoJSONUrl, center_p, zoom_p) {
    
/* GeoJSON Loader --------------------------------------------------- */

    var mapConf = {
		layers: [
			new TileLayer({
				source: new OSM()
			}),
		    new VectorLayer({
		    	title: 'Cartographie des caméras',
		    	source: new VectorSource({
		    		url: geoJSONUrl,
			    	format: new GeoJSON()
				})
		    })
		],
		controls: [],
		target: 'mapView'
    };
    var map = new Map(mapConf);

    map.getView().setCenter(Proj.transform(center_p, 'EPSG:4326', 'EPSG:3857'));
    map.getView().setZoom(zoom_p);
    
/* Displayer -------------------------------------------------------- */
    
    function displayCamera(_id, url, country, city, zip) {
		var el = document.getElementById('cameras');
		
		if (city == '')
		    city = 'Unknown city';
		if (zip == 0)
		    zip = 'Unknown zip';
		if (country == undefined)
		    country = '?';
		el.innerHTML += '<a href="/camera/' + _id + '#videoFluxBox" class="list-group-item" target="_blank"><span class="badge badge-secondary">' + country + '</span> ' + city + ' - ' + zip + '</a>';
    }
    
/* Events ----------------------------------------------------------- */
    
    function showCamerasFromPointer(event) {
		var coordinate = event.coordinate;
		var pixel = map.getPixelFromCoordinate(coordinate);
		var el = document.getElementById('cameras');

		el.innerHTML = '';
		el.style.display = 'block';
		map.forEachFeatureAtPixel(pixel, function(feature) {
		    displayCamera(feature.get('_id'),
				  feature.get('url'),
				  feature.get('country'),
				  feature.get('city'),
				  feature.get('zip'));
		});
    }

    map.on('click', showCamerasFromPointer);
}
